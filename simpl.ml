let rec simpl e =
	match e with
	| Const c -> Const c
	| V -> V
	| Poly l -> Poly l
	| Add (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| (Const a, Const b) 
				-> Const (a +. b)
			| (Const a, V) | (V , Const a)
				-> Poly (a :: [1.0]) 
			| (Const a, Poly l) | (Poly l, Const a) ->
				match l with
				| [] -> Const (a)
				| [x] -> Const (a +. x)
				| x :: xs -> Poly ((x +. a)::xs) ;
			| (V, V) -> Poly ([0.0; 2.0])
			| (V, Poly l) | (Poly l, V) ->
				match l with
				| [] -> V
				| [x] -> Poly (x :: [1.0])
				| x :: (y :: ys) -> Poly ([x] @ ((y +. 1) :: ys))
			| (Poly l1, Poly l2) -> 
				Poly(add_lists l1 l2)
	| Sub (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| (Const a, Const b) 
				-> Const (a -. b)
			| (Const a, V)
				-> Poly ([-a; 1.0])
			| (V , Const a) 
				-> Poly ([a; -1.0])
			| (Const a, Poly l) -> 
				match l with
				| [] -> Const (a)
				| [x] -> Const (a -. x)
				| x :: xs -> Poly ((a -. x) :: xs)
			| (Poly l, Const a) ->
				match l with
				| [] -> Const (-a)
				| [x] -> Const (x -. a)
				| x :: xs -> Poly ((x -. a) :: xs)
			| (V, V) 
				-> Const (0.0)
			| (V, Poly l) ->
				match l with
				| [] -> Poly ([0.0;1.0])
				| [x] -> Poly (-x :: [1.0])
				| x :: (y :: ys) ->
					Poly ((-x :: [1 -. y]) @ (List.rev_map (fun x -> -x) ys))
					(*rev_map is more efficient*)
			| (Poly l, V) ->
				match l with
				| [] -> Poly ([0.0;-1.0])
				| [x] -> Poly (x :: [-1.0])
				| x :: (y :: ys) -> Poly ([x] @ ((y -. 1) :: ys))
			| (Poly l1, Poly l2) -> 
				Poly (sub_lists l1 l2)
	| Mult (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| (Const a, Const b) 
				-> Const (a *. b)
			| (Const a, V) | (V , Const a) 
				-> Poly ([0.0; a])
			| (Const a, Poly l) | (Poly l, Const a) ->
				match l with
				| [] -> Const (a)
				| [x] -> Const (x *. a)
				| l' -> Poly (List.rev_map (fun x -> x *. a) l')
			| (V, V) 
				-> Poly ([0.0;0.0;1.0])
			| (V, Poly l) | (Poly l, V) ->
				match l with
				| [] -> V
				| [x] -> Poly (x :: [1.0])
				| l' -> Poly ([0.0] :: l')
			| (Poly l1, Poly l2) -> 
				Mult(x', y')
	| Div (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| (Const a, Const b) 
				-> Const (a /. b)
			| (Const a, V) | (V , Const a) 
				-> Div(x', y')
				(*Ignores the divide by 0 problem*)
			| (Const a, Poly l) | (Poly l, Const a) ->
				if a <> 0 then
					match l with
					| [] -> Const (a)
					| [x] -> Const (a /. x)
					| x :: xs -> Poly ((x /. a) :: xs)
				else failwith "simpl: cannot divide by 0"
			| (V, V) 
				-> Const (1.0)
			| (V, Poly l) | (Poly l, V) | (Poly l1, Poly l2) 
				-> Div(x', y')
;;
