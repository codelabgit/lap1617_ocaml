(* Expressions module body *)

(* 
Aluno 1: Andre Rodrigues, n 47806
Aluno 2: Jose Duarte, n 47288

Comment:

?????????????????????????
?????????????????????????
?????????????????????????
?????????????????????????
?????????????????????????
?????????????????????????

*)


(*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
   80 columns
*)

type exp =
		Add of exp * exp
	| Sub of exp * exp
	| Mult of exp * exp
	| Div of exp * exp
	| Const of float
	| V
	| Poly of float list
;;

let epsilon = 0.000001;;
let step = 0.1;;

let feq f1 f2 =
	abs_float(f1 -. f2) <= epsilon
;;

(* Aux function to calc the number of non-zeros in the list *)
let rec non_zeros l =
	match l with
	| [] -> 0
	| x :: xs ->
			if x <> 0. then 1 + non_zeros xs
			else
				non_zeros xs
;;

(* Alternate implementation, must choose which is better *)
let non_zeros' l =
	let isNonZero acc v =
		if v <> 0. then acc +1 else acc in
	List.fold_left isNonZero 0 l
;;

(* Aux function to calc the number of consecutive zeros until the end of   *)
(* the list                                                                *)
let rec consec_zeros l =
	match l with
	| [] -> 0
	| x :: xs ->
			if x = 0. && (non_zeros xs) = 0 then
				1 + List.fold_left (fun acc _ -> acc + 1) 0 xs
			else consec_zeros xs
;;

let pcount l =
	let rec aux nz fz l' =
		match l' with
		| [] -> (nz, fz)
		| [x] -> if x = 0. then (nz, fz + 1) else (nz + 1, fz)
		| x :: ((y :: _) as xs) -> 
			match x, y with
			| 0., 0. -> aux nz (fz + 1) xs
			| 0., _ -> aux nz 0 xs
			| _, 0. -> aux (nz + 1) 0 xs
			| _,_ -> aux (nz + 1) 0 xs
	in aux 0 0 l
;;

(* Tail recursive *)
let rec pcount' l =
    match l with
    |[]->(0,0)
    |x :: xs->let (a,b) = pcount' xs in
        if x <> 0.0 then (a+1,b)
    	else if x = 0.0 && a = 0 then (a, b+1)
  		else (a,b) 
;;

let rec size e =
	match e with
	| Const _ -> 1 
	| V -> 1 
	| Poly l -> let (nz, fz) = pcount l in nz + fz + 1
	| Add (e1, e2) | Sub (e1, e2) | Mult (e1, e2) | Div (e1, e2) -> 
		1 + size e1 + size e2 
;;


(* Aux function to calculate the polynomial *)
let rec calc_poly v l index =
	let f_index = float_of_int index in
	match l with
	| [] -> 0.
<<<<<<< HEAD
	| x :: xs ->
=======
	| x:: xs ->
>>>>>>> deriv_implementation
			(x *. (v ** f_index)) +. calc_poly v xs (index + 1)
;;

let rec eval v e =
	match e with
	| Const x -> x
	| V -> v
	| Add (e1, e2) -> (eval v e1) +. (eval v e2)
	| Sub (e1, e2) -> (eval v e1) -. (eval v e2)
	| Mult (e1, e2) -> (eval v e1) *. (eval v e2)
	| Div (e1, e2) -> (eval v e1) /. (eval v e2)
	| Poly l -> calc_poly v l 0
;;

<<<<<<< HEAD
let rec deriv e =
=======
let deriv e =
>>>>>>> deriv_implementation
	match e with
	| Const a -> Const 0.0
	| V -> Const 1.0
	| Add (x,y) -> Add (deriv x, deriv y)
	| Sub (x,y) -> Sub (deriv x, deriv y)
<<<<<<< HEAD
	| Mult (x,y) -> Add (Mult (deriv x, y), Mult (x, deriv y))
	| Div (x,y) ->
		Div (Sub (Mult (deriv x, y), Mult (x, deriv y)), y ** 2.0)
	| Poly l ->
		match l with
		| [] -> Poly []
		| x::xs -> Poly (List.fold_left (fun acc v -> (float_of_int acc) *. v) 1 xs)

=======
	| _ ->
		let x' = deriv x in
		let y' = deriv y in
		match e with
  	| Mult (x,y) ->
  		match x, y with
  		| Const a, Const b -> Const 0.0
  		| Const a, V | V, Const a ->
  			Add (Mult (Const 0.0, V), Mult (Const a, Const 1.0))
  		| V, V -> Mult (Const 2.0, V)
  			
  	| Div (x,y) ->
  		match x, y with
  		| Const a, Const b -> Const 0.0
  		| V, Const a -> Const (1.0 /. a)
			| Const a, V -> Const 0.0 (* Ainda nao esta implementado *)
			|	V, V -> Const 0.0

		| Poly l ->
			match l with
			| [] -> []
			| x::xs -> List.fold_left (fun acc v -> acc *. v) 0 xs
>>>>>>> deriv_implementation
;;

let alike a n e1 e2 =
	failwith "alike: not yet defined"
;;

let newton s e =
	failwith "newton: not yet defined"
;;

<<<<<<< HEAD
let rec add_lists l1 l2 =
	match l1, l2 with
	| [], [] -> []
	| [], x :: xs | x::xs, [] -> x::xs
	| x::xs, y::ys -> [x +. y] @ add_lists xs ys 
;;

let rec sub_lists l1 l2 =
	match l1, l2 with
	| [], [] -> []
	| [], x :: xs | x::xs, [] -> x::xs
	| x::xs, y::ys -> [x -. y] @ add_lists xs ys 
=======
let rec f_to_lists f l1 l2 =
	match l1, l2 with
	| [], [] -> []
	| [], [x] | [x], [] -> [x]
	| [], x::xs | x:xs, [] -> x::xs
	| [x], [y] -> (f x y)::[]
	| [x], y::ys -> (f x y)::ys
	| x::xs, [y] -> (f x y)::xs
	| x::xs, y::ys -> (f x y) @ f_to_lists f xs ys
>>>>>>> deriv_implementation
;;

let rec simpl e =
	match e with
	| Const c -> Const c
	| V -> V
	| Poly l -> Poly l
	| Add (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| Const a, Const b 
				-> Const (a +. b)
			| Const a, V | V , Const a 
<<<<<<< HEAD
				-> Poly (a :: [1.0]) 
=======
				-> Poly a::[1.0] 
>>>>>>> deriv_implementation
				(*Se a = 0 então Poly é menor em size que Add(Const 0.0, V)*)
			| Const a, Poly l | Poly l, Const a ->
				match l with
				| [] -> Const (a)
				| [x] -> Const (a +. x)
<<<<<<< HEAD
				| x :: xs -> Poly (x +. a)::xs
			| V, V -> Poly [0.0; 2.0]
			| V, Poly l | Poly l, V ->
				match l with
				| [] -> V
				| [x] -> Poly x :: [1.0]
				| x :: (y :: ys) -> Poly [x] @ ((y +. 1) :: ys)
			| Poly l1, Poly l2 -> 
				add_lists l1 l2
=======
				| x::xs -> Poly (x +. a)::xs
			| V, V 
				-> Poly [0;2]
			| V, Poly l | Poly l, V ->
				match l with
				| [] -> V
				| [x] -> Poly x::[1.0]
				| x::xs -> Poly [x] @ ((y +. 1)::ys)
			| Poly l1, Poly l2 -> 
				f_to_lists +. l1 l2
>>>>>>> deriv_implementation
	| Sub (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| Const a, Const b 
				-> Const (a -. b)
			| Const a, V
				-> Poly [-a; 1.0]
			| V , Const a 
				-> Poly [a; -1.0] 
				(*Se a = 0 então Poly é menor em size que Add(Const 0.0, V)*)
			| Const a, Poly l -> 
<<<<<<< HEAD
				match l with
				| [] -> Const (a)
				| [x] -> Const (a -. x)
				| x :: xs -> Poly (a -. x) :: xs
=======
			match l with
				| [] -> Const (a)
				| [x] -> Const (a -. x)
				| x::xs -> Poly (a -. x)::xs
>>>>>>> deriv_implementation
			| Poly l, Const a ->
				match l with
				| [] -> Const (-a)
				| [x] -> Const (x -. a)
<<<<<<< HEAD
				| x :: xs -> Poly (x -. a) :: xs
=======
				| x::xs -> Poly (x -. a)::xs
>>>>>>> deriv_implementation
			| V, V 
				-> Const 0.0
			| V, Poly l ->
				match l with
<<<<<<< HEAD
				| [] -> Poly [0.0;1.0]
				| [x] -> Poly -x :: [1.0]
				| x :: (y :: ys) ->
					Poly (-x :: [1 -. y]) @ (List.rev_map (fun x -> -x) ys)
					(*rev_map is more efficient*)
			| Poly l, V ->
				match l with
				| [] -> Poly [0.0;-1.0]
				| [x] -> Poly x :: [-1.0]
				| x :: (y :: ys) -> Poly [x] @ ((y -. 1) :: ys)
			| Poly l1, Poly l2 -> 
				sub_lists l1 l2
=======
				| [] -> V
				| 
			| Poly l, V ->
				match l with
				| [] -> Poly [0.0;-1.0]
				| [x] -> Poly x::[1.0]
				| x::xs -> Poly [x] @ ((y -. 1)::ys)
			| Poly l1, Poly l2 -> 
				f_to_lists -. l1 l2
>>>>>>> deriv_implementation
	| Mult (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| Const a, Const b 
				-> Const (a *. b)
			| Const a, V | V , Const a 
<<<<<<< HEAD
				-> Poly [0.0; a]
=======
				-> Poly a::[1.0] 
>>>>>>> deriv_implementation
				(*Se a = 0 então Poly é menor em size que Add(Const 0.0, V)*)
			| Const a, Poly l | Poly l, Const a ->
				match l with
				| [] -> Const (a)
<<<<<<< HEAD
				| [x] -> Const (x *. a)
				| l' -> Poly (List.rev_map (fun x -> x *. a) l')
			| V, V 
				-> Poly [0.0;0.0;1.0]
			| V, Poly l | Poly l, V ->
				match l with
				| [] -> V
				| [x] -> Poly x :: [1.0]
				| l' -> Poly [0.0] :: l'
			| Poly l1, Poly l2 -> 
				Mult(x', y')
=======
				| [x] -> Const (a *. x)
				| x::xs -> Poly (x *. a)::xs
			| V, V 
				-> Poly [0;2]
			| V, Poly l | Poly l, V ->
				match l with
				| [] -> V
				| [x] -> Poly x::[1.0]
				| x::xs -> Poly [x] @ ((y *. 1)::ys)
			| Poly l1, Poly l2 -> 
				f_to_lists *. l1 l2
>>>>>>> deriv_implementation
	| Div (x, y) -> 
		let x' = simpl x in
		let y' = simpl y in
			match x', y' with
			| Const a, Const b 
				-> Const (a /. b)
			| Const a, V | V , Const a 
<<<<<<< HEAD
				-> Div(x', y')
				(*Ignores the divide by 0 problem*)
			| Const a, Poly l | Poly l, Const a ->
				if a <> 0 then
					match l with
					| [] -> Const (a)
					| [x] -> Const (a /. x)
					| x :: xs -> Poly (x /. a) :: xs
				else failwith "simpl: cannot divide by 0"
			| V, V 
				-> Const 1.0
			| V, Poly l | Poly l, V | Poly l1, Poly l2 -> 
				Div(x', y')
=======
				-> Poly a::[1.0] 
				(*Se a = 0 então Poly é menor em size que Add(Const 0.0, V)*)
			| Const a, Poly l | Poly l, Const a ->
				match l with
				| [] -> Const (a)
				| [x] -> Const (a /. x)
				| x::xs -> Poly (x /. a)::xs
			| V, V 
				-> Poly [0;2]
			| V, Poly l | Poly l, V ->
				match l with
				| [] -> V
				| [x] -> Poly x::[1.0]
				| x::xs -> Poly [x] @ ((y /. 1)::ys)
			| Poly l1, Poly l2 -> 
				f_to_lists /. l1 l2
	| _ -> V
>>>>>>> deriv_implementation
;;

let graph nx ny s e =
	failwith "graph: not yet defined"
;;

(* Tests *)
(* pcount *)
let l1 = [0.0; 1.0; 2.0; 3.0];;
let l2 = [0.0; 1.0; 2.0; 3.0; 0.0; 0.0];;
let l3 = [0.0; 1.0; 2.0; 3.0; 0.0; 0.0; 4.0; 0.0];;

assert(pcount l1 = (3,0));;
assert(pcount l2 = (3,2));;
assert(pcount l3 = (4,1));;

(* size *)
let exp1 = Const 5.0;;
let exp2 = V;;
let exp3 = (Add (Const 5.0, Const 0.0));;
let exp4 = (Mult (V, Add (Const 5.0, Const 0.0)));;
let exp5 = (Mult (V, Add (Const 5.0, Poly [0.0; 1.0; 2.0; 3.0; 0.0; 0.0])));;

let poly1 = Poly [];;
let poly2 = Poly [1.0; 2.0; 3.0];;
let poly3 = Poly [0.0; 1.0; 0.0; 0.0; 2.0; 0.0; 3.0];;
let poly4 = Poly [1.0; 2.0; 3.0; 0.0; 0.0];;

assert(size exp1 = 1);;
assert(size exp2 = 1);;
assert(size exp3 = 3);;
assert(size exp4 = 5);;
assert(size exp5 = 10);;

assert(size poly1 = 1);;
assert(size poly2 = 4);;
assert(size poly3 = 4);;
assert(size poly4 = 6);;
