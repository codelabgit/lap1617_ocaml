(* 
Aluno 1: Andre Rodrigues, n 47806
Aluno 2: Jose Duarte, n 47288

Todas as funcoes do enunciado foram implementadas
*)

type exp =
		Add of exp * exp
	| Sub of exp * exp
	| Mult of exp * exp
	| Div of exp * exp
	| Const of float
	| V
	| Poly of float list
;;

let epsilon = 0.000001;;
let step = 0.1;;

let feq f1 f2 =
	abs_float(f1 -. f2) <= epsilon
;;


let rec pcount l =
    match l with
    |[]->(0,0)
    |x :: xs->let (a,b) = pcount xs in
        if x <> 0.0 then (a+1,b)
    	else if x = 0.0 && a = 0 then (a, b+1)
  		else (a,b) 
;;

let rec size e =
	match e with
	| Const _ -> 1 
	| V -> 1 
	| Poly l -> let (nz, fz) = pcount l in nz + fz + 1
	| Add (e1, e2) | Sub (e1, e2) | Mult (e1, e2) | Div (e1, e2) -> 
		1 + size e1 + size e2 
;;

(* Aux function to calculate the polynomial *)
let rec calc_poly v l index =
	let f_index = float index in
	match l with
	| [] -> 0.
	| x :: xs ->
			(x *. (v ** f_index)) +. calc_poly v xs (index + 1)
;;

let rec eval v e =
	match e with
	| Const x -> x
	| V -> v
	| Add (e1, e2) -> (eval v e1) +. (eval v e2)
	| Sub (e1, e2) -> (eval v e1) -. (eval v e2)
	| Mult (e1, e2) -> (eval v e1) *. (eval v e2)
	| Div (e1, e2) -> (eval v e1) /. (eval v e2)
	| Poly l -> calc_poly v l 0
;;

let rec deriv e =
	match e with
	| Const a -> Const 0.0
	| V -> Const 1.0
	| Add (x,y) -> Add (deriv x, deriv y)
	| Sub (x,y) -> Sub (deriv x, deriv y)
	| Mult (x,y) -> Add (Mult (deriv x, y), Mult (x, deriv y))
	| Div (x,y) ->
		Div (Sub (Mult (deriv x, y), Mult (x, deriv y)), Mult (y, y))
	| Poly l ->
		match l with
		| [] -> Poly []
		| x::xs -> 
			let rec deriv_poly l expo =
				match l with
				| [] -> []
				| y::ys -> (y *. float expo)::deriv_poly ys (expo + 1)
			in Poly (deriv_poly xs 1)
;;

let rec alike a n e1 e2 =
	if n = 0 && feq (eval a e1) (eval a e2) then true
	
	else if not (feq (eval a e1) (eval a e2)) then false
	
	else
		alike (a +. (float n -. 1.0) *. step) (n-1) e1 e2
;;

let rec newton s e =
	let res = s -. ((eval s e) /. (eval s (deriv e))) in
	if feq (eval res e) 0.0 then res
	else newton res e
;;

let rec add_lists l1 l2 =
	match l1, l2 with
	| [], [] -> []
	| [], x :: xs | x::xs, [] -> x::xs
	| x::xs, y::ys -> [x +. y] @ add_lists xs ys 
;;

let rec sub_lists l1 l2 =
	match l1, l2 with
	| [], [] -> []
	| [], x :: xs | x::xs, [] -> x::xs
	| x::xs, y::ys -> [x -. y] @ add_lists xs ys 
;;

let rec clear_zeros l = 
	let l_rev = List.rev l in
	match l_rev with
	| [] -> []
	| x::xs -> if feq x 0.0 then clear_zeros (List.rev xs) else (List.rev (x::xs))
;;

let rec convert e = 
	match e with
	| Poly [] -> Const 0.0
	| Poly [x] -> Const x
	| Poly [0.0; 1.0] -> V
	| Poly l -> Poly (clear_zeros l)
	| Add (x, y) -> Add(convert x, convert y)
	| Sub (x, y) -> Sub(convert x, convert y)
	| Mult (x, y) -> Mult(convert x, convert y)
	| Div (x, y) -> Div(convert x, convert y)
	| _ -> e
;;

let rec aux_simpl e =
	(match e with
	| Const c -> Poly [c]
	| V -> Poly [0.0; 1.0]
	| Poly l -> Poly l
	| Add(x, y) -> 
		let x' = aux_simpl x in
		let y' = aux_simpl y in
		(match x', y' with
		| Poly [], Poly [] -> Poly []
		| Poly [], Poly l | Poly l, Poly [] -> Poly l
		| Poly l1, Poly l2 -> Poly (add_lists l1 l2)
		| _, _ -> Add(x', y')
		)
	| Sub(x, y) -> 
		let x' = aux_simpl x in
		let y' = aux_simpl y in
		(match x', y' with
		| Poly [], Poly [] -> Poly []
		| Poly [], Poly l -> Poly (List.map (fun x -> -.x) l)
		| Poly l, Poly [] -> Poly l
		| Poly l1, Poly l2 -> Poly (add_lists l1 l2)
		| _, _ -> Sub(x', y')
		)
	| Mult(x, y) -> 
		let x' = aux_simpl x in
		let y' = aux_simpl y in
		(match x', y' with
		| Poly [], Poly [] -> Poly [] 
		| Poly [], Poly l | Poly l, Poly [] -> Poly []
		| Poly l1, Poly l2 -> 
			(match l1, l2 with
			| [x], [y] -> Poly ([x *. y])
			| [x], l | l, [x] -> Poly (List.map (fun y -> y *. x) l)
			| [0.0; y1], [0.0; y2] -> Poly ([0.0; 0.0; y1 *. y2])
			| [0.0; x], l | l, [0.0; x] -> Poly (0.0::((List.map (fun y -> y *. x) l)))
			| _, _ -> Mult(Poly l1, Poly l2)
			)
		| _, _ -> Mult(x', y')
		)
	| Div(x, y) ->
		let x' = aux_simpl x in
		let y' = aux_simpl y in
		(match x', y' with
		| Poly [], Poly [] -> failwith "aux_simpl: divide by zero"
		| Poly l, Poly [] -> failwith "aux_simpl: divide by zero"
		| Poly [], Poly l -> Poly []
		| Poly l1, Poly l2 -> 
			(match l1, l2 with
			| [x], [y] -> Poly ([x /. y])
			| [x], l | l, [x] -> Poly (List.map (fun y -> y /. x) l)
			| _, _ -> Div(Poly l1, Poly l2)
			)
		| _, _ -> Div(x', y')
		)
	)
;;

let simpl e =
	let simpl_exp = aux_simpl e in
		convert simpl_exp
;;

let near valueY markingY stepY =
    let bottomY = markingY -. stepY /. 2.0 in
        let topY = markingY +. stepY /. 2.0 in
            bottomY < valueY && valueY <= topY
;;

let rec get_window_y nx s e (minY, maxY) =
	let eval_val = eval s e in
	let y_min = min minY eval_val in
	let y_max = max maxY eval_val in
	
	if nx = 0 then
		(minY, maxY)
	else 
		get_window_y (nx - 1) (s +. step) e (y_min, y_max)
;;

let graph_string maxY stepY line nx s e = 
	let get_markingY = maxY -. (stepY *. float_of_int line) in
	let y_col = int_of_float ((abs_float s) /. step) in
	String.init nx (fun x -> 
		let y_coord = eval (s +. ((float_of_int x) *. step)) e in
		if (near y_coord get_markingY stepY) 
			then '*'
		else if x = y_col
			then '|'	
		else if near 0.0 get_markingY stepY
			then '-'
		else ' ')
;;

let graph nx ny s e =
	let min_max_Y = get_window_y nx s e (0.0, 0.0) in
		let minY, maxY = min_max_Y in
		let stepY = (maxY -. minY) /. float (ny - 1) in
			let rec graph_list n_line =
			if n_line = (ny - 1) then
				[(graph_string maxY stepY n_line nx s e)]
			else 
				(graph_string maxY stepY n_line nx s e)
				:: (graph_list (n_line + 1))
			in graph_list 0
;;

