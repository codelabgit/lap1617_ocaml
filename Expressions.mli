type exp 
val epsilon : float
val step : float
val feq : float -> float -> bool
val pcount : float list -> int * int
val size : 'a -> 'b
val eval : float -> exp -> float
val deriv : 'a -> 'b
val alike : 'a -> 'b -> 'c -> 'd -> 'e
val newton : 'a -> 'b -> 'c
val simpl : 'a -> 'b
val graph : 'a -> 'b -> 'c -> 'd -> 'e
